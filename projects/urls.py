from django.urls import path
from .views import Projectview, Projectdetailview, Createproject


app_name = "projects"

urlpatterns = [
    path("", Projectview.as_view(), name="list_projects"),
    path(
        "project/<int:id>/", Projectdetailview.as_view(), name="show_project"
    ),
    path("create/", Createproject.as_view(), name="create_project"),
]
