from django.shortcuts import render
from django.views import View
from .models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy


class Projectview(LoginRequiredMixin, View):
    def get(self, request):
        user = request.user
        projects = Project.objects.filter(owner=user)
        context = {"projects": projects}
        return render(request, "project_list.html", context)


class Projectdetailview(LoginRequiredMixin, View):
    def get(self, request, id):
        project = get_object_or_404(Project, id=id)
        context = {"project": project, "tasks": project.tasks.all()}
        return render(request, "project_detail_view.html", context)


class Createproject(LoginRequiredMixin, CreateView):
    model = Project
    fields = ["name", "description", "owner"]
    success_url = reverse_lazy("list_projects")
    template_name = "create.html"

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)
