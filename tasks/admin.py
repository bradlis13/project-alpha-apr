from django.contrib import admin
from .models import Task


admin.site.register(Task)


class TaskAdmin(admin.ModelAdmin):
    list_display = ("name", "start_date", "due_date", "is_completed")
    list_filter = ("is_completed", "project", "assignee")
    search_fields = ("name",)


# Register your models here.
