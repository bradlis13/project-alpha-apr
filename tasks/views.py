from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.urls import reverse_lazy
from .models import Task
from django.contrib.auth.mixins import LoginRequiredMixin


class CreateTaskView(LoginRequiredMixin, CreateView):
    model = Task
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    success_url = reverse_lazy("list_projects")
    template_name = "create_task.html"

    def form_valid(self, form):
        return super().form_valid(form)


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "show_my_tasks.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


# Create your views here.
